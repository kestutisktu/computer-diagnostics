package com.martylabs.computerdiagnostics.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.martylabs.computerdiagnostics.Models.Answer;
import com.martylabs.computerdiagnostics.Models.Question;
import com.martylabs.computerdiagnostics.R;

import java.util.ArrayList;

/**
 * Created by Martynas on 2015.07.01.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    String childText;
    private Context _context;
    ArrayList<Question> questions;

    public ExpandableListAdapter(Context context,ArrayList<Question> questions){
        this._context = context;
        this.questions = questions;

    }

    @Override
    public Answer getChild(int groupPosition, int childPosititon) {
        return questions.get(groupPosition).getAnswers().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();
        childText = getChild(groupPosition, childPosition).getAnswer();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.answer_layout, null);
        }

        viewHolder.textAnswer = (RadioButton) convertView
                .findViewById(R.id.ChildItem);

        //convertView.setClickable(false);
        //viewHolder.textAnswer.setFocusable(false);
        viewHolder.textAnswer.setChecked(getChild(groupPosition, childPosition).isSelected());
        viewHolder.textAnswer.setText(childText);
        return convertView;
    }
    //public void addItem(final Question question) {
    //    questions.add(question);
    //    notify();
    //}

    @Override
    public int getChildrenCount(int groupPosition) {
        //return this._listDataChild.get(this._listDataHeader.get(groupPosition))
        //        .size();
        return questions.get(groupPosition).getAnswers().size();
    }


    @Override
    public Question getGroup(int groupPosition) {
        //return this._listDataHeader.get(groupPosition);
        return questions.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        //return this._listDataHeader.size();
        return questions.size();
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.question_layout, null);
        }

        ViewHolder viewHolder = new ViewHolder();


        viewHolder.titleQuestion = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        viewHolder.titleQuestion.setText(getGroup(groupPosition).getTitle(), TextView.BufferType.NORMAL);
        viewHolder.selectedAnswer = (TextView)convertView.findViewById(R.id.lblListHeaderSelected);
        if(!getGroup(groupPosition).getAnswer().equals("")) {
            viewHolder.selectedAnswer.setVisibility(View.VISIBLE);
            viewHolder.selectedAnswer.setText(getGroup(groupPosition).getAnswer());
        }
        else{ viewHolder.selectedAnswer.setVisibility(View.GONE);}
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolder {
        RadioButton textAnswer;
        TextView titleQuestion;
        TextView selectedAnswer;
    }

}
