package com.martylabs.computerdiagnostics;

import android.app.Activity;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Martynas on 2015.07.01.
 */
public class BeepListener extends Activity{
    boolean lock = true;
    boolean detected = false;
    ArrayList<Integer> soundAmpArray = new ArrayList<>();
    MediaRecorder recorder;
    @Override
    public void onCreate(Bundle savedInsatnce){
        super.onCreate(savedInsatnce);
        setContentView(R.layout.beep_listener_layout);

        recorder = new MediaRecorder();
    }


    public class RecordThread extends Thread{
        public void run(){
            try {
                while (!detected) {
                    lock = true;
                    soundAmpArray.add(recorder.getMaxAmplitude());
                    lock = false;
                    Thread.sleep(100);
                }
            }catch(InterruptedException e){
                Log.d("record thread", "interrupted");
            }
        }
    }
    public class ProccessThread extends Thread{
        public void run(){
            while(!detected) {
                if (!lock) {
                    if (soundAmpArray.size() == 10) {
                        detected = true;
                    }
                }
            }
        }
    }
}