package com.martylabs.computerdiagnostics.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.martylabs.computerdiagnostics.MainActivity;
import com.martylabs.computerdiagnostics.Models.Constants;
import com.martylabs.computerdiagnostics.Models.Setting;
import com.martylabs.computerdiagnostics.R;
import com.martylabs.computerdiagnostics.SplashScreenActivity;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Martynas on 2015.07.23.
 */
public class AboutFragment extends Fragment {
    public String[] HEADERS, TITLES, SUMMARIES;

    Double onlineVersion;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    CheckOnlineVersion checkOnlineVersion;
    private static final Integer LIST_HEADER = 0;
    private static final Integer LIST_ITEM = 1;
    Context context;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ListView lv = (ListView) view.findViewById(R.id.listView1);
        lv.setAdapter(new MyListAdapter(getActivity(), R.id.listView1, fillList()));
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HEADERS = getResources().getStringArray(R.array.about_headers);
        TITLES = getResources().getStringArray(R.array.about_titles);
        SUMMARIES = getResources().getStringArray(R.array.about_summaries);
        checkOnlineVersion = new CheckOnlineVersion();
        checkOnlineVersion.start();



    }

    @Override
    public void onAttach(Activity activity) {
        context = activity;
        super.onAttach(activity);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
        editor = sharedPreferences.edit();
    }

    public ArrayList<Setting> fillList() {
        ArrayList<Setting> settings = new ArrayList<Setting>();
        for (int i = 0; i < HEADERS.length + TITLES.length; i++) {
            Setting singleSetting = new Setting();
            String headerText;
            switch (i) {
                case 0:
                case 3:
                case 8:
                    headerText = HEADERS[i / 3];
                    break;
                default:
                    headerText = "";
                    break;
            }
            int pos = 0;
            if (i > 0 && i < 3) {
                pos = i - 1;
            } else if (i > 3 && i < 8) {
                pos = i - 2;
            } else if (i > 8) {
                pos = i - 3;
            }
            if (!headerText.equals("")) {
                singleSetting.setHeader(headerText);
                settings.add(singleSetting);
            } else {
                singleSetting.setTitle(TITLES[pos]);
                singleSetting.setSummary(SUMMARIES[pos]);
                settings.add(singleSetting);
            }
        }
        return settings;
    }

    public class MyListAdapter extends ArrayAdapter<Setting> {

        public final Context mContext;
        ViewHolder holder;

        public MyListAdapter(Context context, int resource, List objects) {
            super(context, resource, objects);
            mContext = context;
        }

        @Override
        public int getCount() {
            return HEADERS.length + TITLES.length;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {

            if (!getItem(position).getHeader().equals("")) {

                View item = convertView;
                if (convertView == null || convertView.getTag() == LIST_ITEM) {

                    item = LayoutInflater.from(mContext).inflate(
                            R.layout.settings_about_list_header_layout, parent, false);
                    item.setTag(LIST_HEADER);

                }
                TextView headerTextView = (TextView) item.findViewById(R.id.lv_list_hdr);
                headerTextView.setText(getItem(position).getHeader());
                return item;
            } else {
                View item = convertView;
                item = LayoutInflater.from(mContext).inflate(
                        R.layout.settings_about_list_layout, parent, false);
                item.setTag(LIST_ITEM);

                holder = new ViewHolder();
                holder.title = (TextView) item.findViewById(R.id.lv_item_header);
                holder.title.setText(getItem(position).getTitle());
                holder.summary = (TextView) item.findViewById(R.id.lv_item_subtext);
                holder.summary.setText(getItem(position).getSummary());

                ProgressBar progressBar = (ProgressBar) item.findViewById(R.id.progressBar2);
                progressBar.setVisibility(View.GONE);
                ImageView imageView = (ImageView) item.findViewById(R.id.imageView2);
                imageView.setVisibility(View.GONE);

                //if (position == 1 || position == 4 || position == 9) {
                //divider.setVisibility(View.VISIBLE);
                //}
                //*/
                if (position == 5) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    Double localVersion = Double.parseDouble(sharedPreferences.getString(Constants.PREFS_QUESTION_VERSION, "0.0"));
                    Log.d("aboutfragment", "local version: " + localVersion);
                    try {
                        checkOnlineVersion.join();
                    }
                    catch(InterruptedException e){}
                    Double serverVersion = onlineVersion;
                    Log.d("aboutfragment", "online version: " + serverVersion);
                    if (serverVersion != 0.0) {
                        if (localVersion >= serverVersion) {
                            Log.d("version", "still no need");
                            imageView.setImageResource(R.mipmap.ic_done_black_24dp);
                            imageView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            holder.summary.setText("Up to date: " + localVersion);
                        } else {
                            Log.d("versions", "need to update");
                            imageView.setImageResource(R.mipmap.ic_warning_black_24dp);
                            imageView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            holder.summary.setText("Outdated: " + localVersion + " Newest is: " + serverVersion);
                        }

                    } else {
                        imageView.setImageResource(R.mipmap.ic_error_black_24dp);
                        imageView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        holder.summary.setText("Connection error. Current version: " + localVersion);
                    }

                }

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String url;
                        Intent i;
                        switch (position) {
                            case 1:
                                Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.fromParts(
                                        "mailto",getResources().getString(R.string.support_email), null));
                                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.support_email_subject));
                                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.support_email_body));
                                startActivity(Intent.createChooser(intent, getString(R.string.send_email_intent)));
                                break;
                            case 2:
                                url = "http://www.martylabs.com";
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                startActivity(i);
                                break;
                            case 5:
                                showExplainingDialog();
                                break;
                            case 6:
                                url = "http://play.google.com";
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                startActivity(i);
                                break;
                        }
                        notifyDataSetChanged();
                    }
                });
                return item;
            }
        }
        private void showExplainingDialog(){
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.notice))
                    .setMessage(context.getString(R.string.questionnaire_update_text) + context.getResources().getString(R.string.later) + " \'")
                    .setPositiveButton(context.getResources().getString(R.string.restart), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(context, SplashScreenActivity.class);
                            startActivity(intent);
                            ((MainActivity) context).finish();
                        }
                    })
                    .setNegativeButton(context.getString(R.string.later), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setIcon(R.mipmap.ic_warning_black_24dp)
                    .setCancelable(false)
                    .show();
        }

    }

    public static class ViewHolder {
        public TextView title;
        public TextView summary;
        public CheckBox checked;
    }

    public class CheckOnlineVersion extends Thread {
        public void run() {
            try {
                JSONObject json = new JSONObject(IOUtils.toString(new URL("http://martylabs.com/pcdiagnostics/checkQuestionVersion.php"), Charset.forName("UTF-8")));
                onlineVersion = Double.parseDouble(json.getString(Constants.ONLINE_QUESTION_VERSION));
            } catch (Exception e) {
                onlineVersion = 0.0;
            }
        }
    }
}

