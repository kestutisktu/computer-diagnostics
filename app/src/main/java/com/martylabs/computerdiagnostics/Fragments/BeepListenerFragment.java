package com.martylabs.computerdiagnostics.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martylabs.computerdiagnostics.R;

/**
 * Created by Martynas on 2015.07.22.
 */
public class BeepListenerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_beep, container, false);
    }
}
