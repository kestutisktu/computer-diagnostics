package com.martylabs.computerdiagnostics.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.martylabs.computerdiagnostics.Adapters.ExpandableListAdapter;
import com.martylabs.computerdiagnostics.MainActivity;
import com.martylabs.computerdiagnostics.Models.Constants;
import com.martylabs.computerdiagnostics.Models.Question;
import com.martylabs.computerdiagnostics.Models.Solution;
import com.martylabs.computerdiagnostics.R;

import java.util.ArrayList;

/**
 * Created by Martynas on 2015.07.22.
 */
public class QuestionnaireFragment extends Fragment {
    ExpandableListView expListView;
    ExpandableListAdapter listAdapter;
    ArrayList<Question> questionsMainArray = new ArrayList<>();
    ArrayList<Question> questionsListViewArray = new ArrayList<>();
    ArrayList<Solution> solutionsArray;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_questionnaire, container, false);
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        listAdapter = new ExpandableListAdapter(getActivity(), questionsListViewArray);
        expListView.setAdapter(listAdapter);

        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                Log.d("group clicked:", groupPosition + "");
                return false;
            }
        });

        expListView.expandGroup(0);

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Log.d("groupexpand", "group pos: " + groupPosition+ " lastvisible:"+expListView.getLastVisiblePosition());

                //expListView.smoothScrollToPosition(expListView.getBottom());
                listAdapter.notifyDataSetChanged();
                //expListView.setSelectedGroup(groupPosition);

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                questionsListViewArray.get(groupPosition).setAnswer(questionsListViewArray.get(groupPosition).getAnswer(childPosition).getAnswer());
                questionsListViewArray.get(groupPosition).getAnswer(childPosition).setSelected(true);
                setAllChildsRadioButtonFalse(groupPosition);
                parent.collapseGroup(groupPosition);
                String pointsTo = questionsListViewArray.get(groupPosition).getAnswers().get(childPosition).getLeadsTo();
                if (groupPosition < questionsListViewArray.size() - 1) {
                    int size = questionsListViewArray.size();
                    for (int i = size - 1; i > groupPosition; i--) {
                        questionsListViewArray.remove(i);
                    }
                    questionsListViewArray.add(questionsMainArray.get(Integer.parseInt(pointsTo)));
                } else if (!pointsTo.equals("")) {
                    if (!pointsTo.contains("solution")) {
                        questionsListViewArray.add(questionsMainArray.get(Integer.parseInt(pointsTo)));
                        listAdapter.notifyDataSetChanged();
                        parent.expandGroup(groupPosition + 1);
                        listAdapter.notifyDataSetChanged();
                    } else {
                        if (pointsTo.contains("beepListener")) {

                        } else {
                            pointsTo = pointsTo.replace("solution-", "");
                            AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle).create();
                            alertDialog.setTitle(getString(R.string.solution));

                            alertDialog.setMessage(solutionsArray.get(Integer.parseInt(pointsTo)).getText());
                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();

                        }
                    }
                }
                listAdapter.notifyDataSetChanged();

                expListView.collapseGroup(groupPosition);

                Log.d("Child click", "grouppos:" + groupPosition + " childpos:" + childPosition + "pointTo:");
                if (!questionsListViewArray.get(groupPosition).getAnswer(childPosition).isSelected()) {
                    Log.d("child", "notradio");
                    questionsListViewArray.get(groupPosition).getAnswer(childPosition).setSelected(true);
                    for (int i = 0; i < questionsListViewArray.get(groupPosition).getAnswers().size(); i++) {
                        if (i != childPosition) {
                            questionsListViewArray.get(groupPosition).getAnswer(i).setSelected(false);
                        }
                    }
                    listAdapter.notifyDataSetChanged();

                }

                expListView.smoothScrollToPosition(expListView.getLastVisiblePosition());
                listAdapter.notifyDataSetChanged();

                return false;

            }


        });
        return view;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        questionsMainArray = getArguments().getParcelableArrayList(Constants.QUESTIONS);
        solutionsArray = getArguments().getParcelableArrayList(Constants.SOLUTIONS);




        questionsListViewArray.add(questionsMainArray.get(0));
    }
    public void setAllChildsRadioButtonFalse(int groupPosition){

    }
    public void RestartQuestionnaire(){
        questionsListViewArray.clear();
        questionsListViewArray.add(questionsMainArray.get(0));
        questionsListViewArray.get(0).getAnswer(0).setSelected(false);
        questionsListViewArray.get(0).getAnswer(1).setSelected(false);
        questionsListViewArray.get(0).setAnswer("");
        listAdapter.notifyDataSetChanged();
    }

}
