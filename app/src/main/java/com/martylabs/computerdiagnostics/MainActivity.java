package com.martylabs.computerdiagnostics;

/**
 * Created by Martynas on 2015.07.01.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.martylabs.computerdiagnostics.Adapters.ExpandableListAdapter;
import com.martylabs.computerdiagnostics.Fragments.AboutFragment;
import com.martylabs.computerdiagnostics.Fragments.BeepListenerFragment;
import com.martylabs.computerdiagnostics.Fragments.BlinkListenerFragment;
import com.martylabs.computerdiagnostics.Fragments.QuestionnaireFragment;
import com.martylabs.computerdiagnostics.Models.Constants;
import com.martylabs.computerdiagnostics.Models.Question;
import com.martylabs.computerdiagnostics.Models.Solution;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    Menu menu;
    Bundle bundle;
    ArrayList<Question> questionsMainArray = new ArrayList<>();
    ArrayList<Question> questionsListViewArray = new ArrayList<>();
    ArrayList<Solution> solutionsArray;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(toolbar == null){
            Log.d("null","null");
        }
        bundle = this.getIntent().getExtras();

        questionsMainArray = bundle.getParcelableArrayList(Constants.QUESTIONS);
        solutionsArray = bundle.getParcelableArrayList(Constants.SOLUTIONS);
        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Set the menu icon instead of the launcher icon.
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.mipmap.ic_menu_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);

       /* AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("").build();
        mAdView.loadAd(adRequest);*/

        try {


            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = (Fragment) QuestionnaireFragment.class.newInstance();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            setTitle(getResources().getString(R.string.questionnaire));
    }
        catch(Exception e){}
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("main local version", sharedPreferences.getString("question_version","0.0"));


    }
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        this.menu = menu;
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    public void selectDrawerItem(MenuItem menuItem) {

        MenuItem item = menu.findItem(R.id.restart);
        Fragment fragment = null;
        Class fragmentClass = null;
        switch(menuItem.getItemId()) {
            case R.id.questionnaireFragment:
                fragmentClass = QuestionnaireFragment.class;
                item.setVisible(true);
                break;
            case R.id.beepListenerFragment:
                //fragmentClass = BeepListenerFragment.class;
                showFeaturePendingDialog();
                //item.setVisible(false);
                break;
            case R.id.blinkWatcherFragment:
                //fragmentClass = BlinkListenerFragment.class;
                showFeaturePendingDialog();
                //item.setVisible(false);
                break;
            case R.id.aboutFragment:
                fragmentClass = AboutFragment.class;
                item.setVisible(false);
                break;
            case R.id.contactUs:
                Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.fromParts(
                        "mailto",getResources().getString(R.string.support_email), null));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.support_email_subject));
                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.support_email_body));
                startActivity(Intent.createChooser(intent, getString(R.string.send_email_intent)));
                break;

            case R.id.shareApp:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
                startActivity(Intent.createChooser(sharingIntent,getString(R.string.share_app_using)));
                break;
            default:
                fragmentClass = QuestionnaireFragment.class;
                Log.d("drawer","first item");;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(bundle);

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        if(fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            mDrawer.closeDrawers();
        }


        // Insert the fragment by replacing any existing fragment


        // Highlight the selected item, update the title, and close the drawer

    }


    @Override
    public void onPause(){
        super.onPause();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                //return true;
                break;
            case R.id.restart:
                QuestionnaireFragment fragment = (QuestionnaireFragment) getSupportFragmentManager().findFragmentById(R.id.flContent);
                fragment.RestartQuestionnaire();
                break;

        }
        //return super.onOptionsItemSelected(item);
        return true;
    }
    @Override
    public void onBackPressed(){
        if(mDrawer.isDrawerOpen(GravityCompat.START)){
            mDrawer.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.action_refresh:
                questionsListViewArray.clear();
                listAdapter.notifyDataSetChanged();
                questionsListViewArray.add(questionsMainArray.get(0));
                questionsListViewArray.get(0).getAnswer(0).setSelected(false);
                questionsListViewArray.get(0).getAnswer(1).setSelected(false);
                questionsListViewArray.get(0).setAnswer("");
                listAdapter.notifyDataSetChanged();
                break;
        }
        return true;
    }*/
    public void showFeaturePendingDialog(){
        new AlertDialog.Builder(MainActivity.this,R.style.AppCompatAlertDialogStyle)
                .setTitle(getString(R.string.feature_pending))
                .setMessage(getString(R.string.feature_pending_message))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.mipmap.ic_warning_black_24dp)
                .setCancelable(false)
                .show();
    }
}
