package com.martylabs.computerdiagnostics.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Martynas on 2015.07.01.
 */
public class Answer implements Parcelable {
    private String answer;
    private boolean selected;
    private String leadsTo;

    public Answer() {
        this.answer = null;
        this.selected = false;
        this.leadsTo = "null";
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getLeadsTo() {
        return leadsTo;
    }

    public void setLeadsTo(String leadsTo) {
        this.leadsTo = leadsTo;
    }

    protected Answer(Parcel in) {
        answer = in.readString();
        selected = in.readByte() != 0x00;
        leadsTo = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(answer);
        dest.writeByte((byte) (selected ? 0x01 : 0x00));
        dest.writeString(leadsTo);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Answer> CREATOR = new Parcelable.Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };
}
