package com.martylabs.computerdiagnostics.Models;

/**
 * Created by Martynas on 2015.07.01.
 */
public class Constants {
    public static final int SOCKET_TIMEOUT = 5000;
    public static final String QUESTIONS = "questions";
    public static final String SOLUTIONS = "solutions";
    public static final String ARRAYS = "arrays";
    public static final int CURSOR_TIMER = 350;
    public static final int TEXT_ADD_INTERVAL = 50;
    public static final int BLINK_TIMES_QUESTION = 3;
    public static final int BLINK_TIMES_ANSWER = 2;
    public static final String PREFS_QUESTION_VERSION = "prefs_question_version";
    public static final String ONLINE_QUESTION_VERSION = "version";

}