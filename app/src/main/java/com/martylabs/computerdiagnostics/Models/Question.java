package com.martylabs.computerdiagnostics.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Martynas on 2015.07.01.
 */
public class Question implements Parcelable {
    private int index;
    private String title;
    private String summary;
    private String answer;
    private ArrayList<Answer> answers;

    public Question() {
        this.index = 0;
        this.title = "";
        this.summary = "";
        this.answer = "";
        this.answers = new ArrayList<>();
    }
    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void addAnswer(Answer answer) {
        this.answers.add(answer);
    }

    public Answer getAnswer(int i) { return this.answers.get(i);}
    @Override
    public String toString(){
        return this.index+" "+this.title+" "+this.summary;
    }

    protected Question(Parcel in) {
        index = in.readInt();
        title = in.readString();
        summary = in.readString();
        answer = in.readString();
        if (in.readByte() == 0x01) {
            answers = new ArrayList<Answer>();
            in.readList(answers, Answer.class.getClassLoader());
        } else {
            answers = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(index);
        dest.writeString(title);
        dest.writeString(summary);
        dest.writeString(answer);
        if (answers == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(answers);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };
}

