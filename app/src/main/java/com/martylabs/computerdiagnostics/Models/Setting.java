package com.martylabs.computerdiagnostics.Models;

/**
 * Created by Martynas on 2015.07.23.
 */
public class Setting {
    String header;
    String title;
    String summary;
    boolean checked;



    public Setting() {
        this.summary = "";
        this.title = "";
        this.header = "";
        this.checked = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}