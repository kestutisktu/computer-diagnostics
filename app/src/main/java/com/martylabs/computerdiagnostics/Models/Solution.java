package com.martylabs.computerdiagnostics.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Martynas on 2015.07.01.
 */
public class Solution implements Parcelable {

    int id;
    String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Solution() {

        this.id = 0;
        this.text = "";
    }
    protected Solution(Parcel in) {
        id = in.readInt();
        text = in.readString();
    }
    @Override
    public String toString(){
        return "ID: "+id+" Text: "+text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(text);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Solution> CREATOR = new Parcelable.Creator<Solution>() {
        @Override
        public Solution createFromParcel(Parcel in) {
            return new Solution(in);
        }

        @Override
        public Solution[] newArray(int size) {
            return new Solution[size];
        }
    };
}
