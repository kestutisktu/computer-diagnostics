package com.martylabs.computerdiagnostics;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.martylabs.computerdiagnostics.Cryptography.SimpleEncryptorDecryptor;
import com.martylabs.computerdiagnostics.Models.Answer;
import com.martylabs.computerdiagnostics.Models.Constants;
import com.martylabs.computerdiagnostics.Models.Question;
import com.martylabs.computerdiagnostics.Models.Solution;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.widget.TextView;

import javax.xml.transform.stream.StreamResult;


/**
 * Created by Martynas on 2015.07.01.
 */
public class SplashScreenActivity extends Activity {
    DownloadXML downloadXML;
    private boolean isUpdateNecessary = false;
    private String updatedVersion;
    private SharedPreferences sharedPreferences;
    ArrayList<Question> questions;
    ArrayList<Solution> solutions;
    SharedPreferences.Editor editor;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        int seconds = Calendar.getInstance().get(Calendar.SECOND) + 3;
        setContentView(R.layout.splash_screen_layout);
        downloadXML = new DownloadXML(this);
        ParseXML parseXML = new ParseXML(this, seconds, downloadXML);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        TextView infoText = (TextView) findViewById(R.id.textView);
        infoText.setText(getString(R.string.loading));
        //downloadXML.start();
        parseXML.start();
        /*CheckQuestionVersion checkQuestionVersion = new CheckQuestionVersion();
        checkQuestionVersion.start();
        try {
            checkQuestionVersion.join();
        }catch(InterruptedException e){}
       if(isUpdateNecessary){
            downloadXML.start();
            parseXML.start();
        }
        else{
            parseXML.start();
        }*/


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int bytesRead;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "SplashScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.martylabs.computerdiagnostics/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "SplashScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.martylabs.computerdiagnostics/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    public class DownloadXML extends Thread {
        int count = 0;
        Context context;
        File tempFile, file;

        public DownloadXML(Context context) {
            this.context = context;
            tempFile = new File(context.getFilesDir(), "temp.xml");
            file = new File(context.getFilesDir(), "questions.xml");
        }

        public void run() {
            try {
                //updateTextViewFromUI(connection,"Connecting to server...");
                String parameters = "username=" + getString(R.string.server_username) + "&password=" + getString(R.string.server_password) + "&language=" + getString(R.string.server_language);
                URL url = new URL(getResources().getString(R.string.server_url));
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestMethod("POST");

                OutputStreamWriter request = new OutputStreamWriter(httpURLConnection.getOutputStream());
                request.write(parameters);
                request.flush();
                request.close();
                httpURLConnection.connect();

                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {

                    Log.d("http", "ok");
                    InputStream input = httpURLConnection.getInputStream();
                    FileOutputStream output = new FileOutputStream(tempFile);
                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();

                    FileOutputStream bos = new FileOutputStream(file);
                    byte[] yourKey = SimpleEncryptorDecryptor.generateKey("password");
                    byte[] fileBytes = SimpleEncryptorDecryptor.encodeFile(yourKey, convertFileToByteArray(tempFile));
                    bos.write(fileBytes);
                    bos.flush();
                    bos.close();
                    boolean success = tempFile.delete();
                    Log.d("tempFile deleted", String.valueOf(success));

                } else {
                    Log.d("http", "rejected");
                }
            } catch (UnknownHostException e) {
                Log.d("uknknowhostexception", "nera rysio");
            } catch (SocketTimeoutException e) {
                Log.d("socketexception", "timeout");
            } catch (IOException e) {
                Log.d("ioexception_download", "ioexception");
            } catch (Exception e) {
                Log.d("exception_download", "exception");
            }
        }
    }

    public class ParseXML extends Thread {

        Context context;
        XmlPullParser parser;
        File file;
        int timeMillis;
        //DownloadXML downloadXML;
        List<String> mLines = new ArrayList<>();

        ParseXML(Context context, int seconds, DownloadXML downloadXML) {
            this.context = context;
            AssetManager am = context.getAssets();

            // InputStream file = mngr.open("questions.xml");
            // file = new File(context.getFilesDir(), "questions.xml");
            /*try {
                InputStream is = am.open("questions.xml");
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null)
                    mLines.add(line);
                //file = new File(context.getAssets().open("questions.xml"));
                // get input stream for text


            } catch (IOException ex) {
                Log.d("ioexception", "blasdasd");
            }*/

            this.timeMillis = seconds;
            // this.downloadXML = downloadXML;
        }


        public void run() {
            AssetManager mngr = getAssets();
            try {
                //downloadXML.join();
                XmlPullParserFactory pullParserFactory;
                pullParserFactory = XmlPullParserFactory.newInstance();
                parser = pullParserFactory.newPullParser();

                InputStream in_s = mngr.open("questions.xml");
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in_s, null);

                ArrayList<Question> questions = parseXMLQuestions(parser);
                ArrayList<Solution> solutions = parseXMLSolutions(parser);
                //Log.d("questions array", questions.toString());
                //Log.d("solutions array", solutions.toString());

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(Constants.QUESTIONS, questions);
                bundle.putParcelableArrayList(Constants.SOLUTIONS, solutions);

                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtras(bundle);

                editor.putString(Constants.PREFS_QUESTION_VERSION, updatedVersion);
                editor.commit();
                while (Calendar.getInstance().get(Calendar.SECOND) < timeMillis) {
                }
                context.startActivity(intent);
                ((SplashScreenActivity) context).finish();

            } catch (XmlPullParserException e) {
                Log.d("xmlexception", "xmlexception");
            } catch (IOException e) {
                Log.d("ioexception", "ioexception");
            } catch (Exception e) {
                Log.d("exception", e.getMessage());
            }
        }


        private ArrayList<Question> parseXMLQuestions(XmlPullParser parser) throws XmlPullParserException, IOException, Exception {
            ArrayList<Question> questions = new ArrayList<>();

            int eventType = parser.getEventType();
            Question currentQuestion = null;
            Answer currentAnswer = null;
            boolean end = false;

            while (eventType != XmlPullParser.END_DOCUMENT && !end) {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_TAG:

                        name = parser.getName();
                        if (name.equals("question")) {
                            currentQuestion = new Question();
                        }
                        if (currentQuestion != null) {
                            switch (name) {
                                case "id":
                                    currentQuestion.setIndex(Integer.parseInt(parser.nextText()));
                                    break;
                                case "title":
                                    currentQuestion.setTitle(parser.nextText());
                                    break;
                                case "summary":
                                    currentQuestion.setSummary(parser.nextText());
                                    break;
                            }
                        }
                        if (name.equals("answer")) {
                            currentAnswer = new Answer();
                        }
                        if (currentAnswer != null) {
                            switch (name) {
                                case "text":
                                    currentAnswer.setAnswer(parser.nextText());
                                    break;
                                case "leadsTo":
                                    currentAnswer.setLeadsTo(parser.nextText());
                                    break;
                                default:
                                    break;
                            }

                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("question") && currentQuestion != null) {
                            questions.add(currentQuestion);
                        }
                        if (name.equalsIgnoreCase("answer") && currentAnswer != null && currentQuestion != null) {
                            currentQuestion.addAnswer(currentAnswer);
                        }
                        if (name.equals("questions")) {
                            end = true;
                        }
                }
                eventType = parser.next();
            }

            return questions;
        }

        private ArrayList<Solution> parseXMLSolutions(XmlPullParser parser) throws XmlPullParserException, IOException {
            ArrayList<Solution> solutions = new ArrayList<>();
            int eventType = parser.getEventType();
            Solution currentSolution = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                Log.d("pull parser", "works");
                String name;
                switch (eventType) {
                    case XmlPullParser.START_TAG:


                        name = parser.getName();
                        Log.d("start tag", name);
                        if (name.equalsIgnoreCase("solution")) {
                            currentSolution = new Solution();
                        }
                        if (currentSolution != null) {
                            switch (name) {
                                case "id":
                                    currentSolution.setId(Integer.parseInt(parser.nextText()));
                                    break;
                                case "solve":
                                    currentSolution.setText(parser.nextText());
                                    break;
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        name = parser.getName();
                        if (name.equalsIgnoreCase("solution") && currentSolution != null) {
                            solutions.add(currentSolution);
                        }
                        break;
                }
                eventType = parser.next();
            }

            return solutions;
        }
    }

    public class CheckQuestionVersion extends Thread {
        CheckQuestionVersion() {

        }

        public void run() {

            try {
                JSONObject json = new JSONObject(IOUtils.toString(new URL("http://martylabs.com/pcdiagnostics/checkQuestionVersion.php"), Charset.forName("UTF-8")));
                isUpdateNecessary = true;
                Double onlineVersion = Double.parseDouble(json.getString(Constants.ONLINE_QUESTION_VERSION));
                Double localVersion = Double.parseDouble(sharedPreferences.getString(Constants.PREFS_QUESTION_VERSION, "0.0"));
                Log.d("splashscreen", "online version: " + onlineVersion.toString());
                Log.d("splashscreen", "local version: " + localVersion.toString());
                if (localVersion >= onlineVersion) {
                    Log.d("splashscreen", "no need to update");
                    isUpdateNecessary = false;
                    updatedVersion = localVersion.toString();
                } else {
                    Log.d("splashscreen", "need to update");
                    updatedVersion = onlineVersion.toString();
                    isUpdateNecessary = true;
                }
            } catch (Exception e) {
                Log.d("splashscreen", "check online version failed");
                if (!sharedPreferences.contains(Constants.PREFS_QUESTION_VERSION)) {
                    isUpdateNecessary = false;
                    updatedVersion = sharedPreferences.getString(Constants.PREFS_QUESTION_VERSION, "0.0");
                    Log.d("splashscreen", "not first time and without internet, version: " + updatedVersion);
                } else {
                    isUpdateNecessary = false;
                    updatedVersion = sharedPreferences.getString(Constants.PREFS_QUESTION_VERSION, "0.0");
                    Log.d("splashscreen", "not first time and without internet, version: " + updatedVersion);
                }
            }
        }

        private void showErrorConnectingDialog() {
            new AlertDialog.Builder(SplashScreenActivity.this, R.style.AppCompatAlertDialogStyle)
                    .setTitle(getString(R.string.cant_connect_to_server))
                    .setMessage(getString(R.string.check_internet_connection))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SplashScreenActivity.this.finish();
                        }
                    })
                    .setIcon(R.mipmap.ic_error_black_24dp)
                    .setCancelable(false)
                    .show();
        }
    }
}
